import skimage.io
from shapely.geometry import LineString

from py_tps.io import TPSFile
from rl_image_warping.utils import *

tpsF = TPSFile.read_file('/home/rfeather/Data/Dorsal/Oxyrhopus_melanogenys_dorsal.TPS')
im0 = tpsF.images[7]
img = skimage.io.imread(f'/home/rfeather/Data/Dorsal/{im0.image}')
curve_1 = im0.curves[1].tps_points.points
curve_1 = curve_1[:,[1,0]]
curve_1[:,0] = img.shape[0] - curve_1[:,0]

curve_0 = im0.curves[0].tps_points.points[:,:]
curve_0 = curve_0[:,[1,0]]
curve_0[:,0] = img.shape[0] - curve_0[:,0]

auto_deoverlap=True
c0_shapley = LineString(curve_0)
c1_shapley = LineString(curve_1)
if c0_shapley.length > c1_shapley.length:
    outer_curve = curve_0
    inner_curve = curve_1
    inward_direction = Side.LEFT_SIDE
else:
    outer_curve = curve_1
    inner_curve = curve_0
    inward_direction = Side.RIGHT_SIDE
if auto_deoverlap:
    deoverlap_direction = other_side(inward_direction)
    inner_curve = deoverlap_curves(outer_curve, inner_curve, deoverlap_direction)