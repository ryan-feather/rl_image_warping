import numpy as np
from numpy import testing as np_test
from scipy.stats import norm
from scipy.spatial import cKDTree
from shapely.geometry import Point, LineString
import cv2

from py_tps.tps_io import TPSImage, TPSCurve, TPSPoints
from rl_image_warping import image_warping as U

NArr = np.array  # make some shorthand for casting to np array to avoid extra verbosity in many repeated operations


def test_euclidean():
    np_test.assert_almost_equal(U.euclidean(NArr([[0, 0]]), NArr([[0, 0]])), 0)
    np_test.assert_almost_equal(U.euclidean(NArr([[1, 0]]), NArr([[0, 0]])), 1)
    np_test.assert_almost_equal(U.euclidean(NArr([[1, 0]]), NArr([[1, 0]])), 0)
    np_test.assert_almost_equal(U.euclidean(NArr([[0, 1]]), NArr([[0, 1]])), 0)
    np_test.assert_almost_equal(U.euclidean(NArr([[-1, -1]]), NArr([[0, 0]])), np.sqrt(2))

    array_0 = np.array([[0, 0], [1, 1]])
    array_1 = np.array([[0, 0], [-1, -1]])
    np_test.assert_array_almost_equal(U.euclidean(array_0, NArr([[0, 0]])), [0, np.sqrt(2)])
    np_test.assert_array_almost_equal(U.euclidean(array_0, array_1), [0, 2 * np.sqrt(2)])


def test_get_all_curve_extents():
    test_im0 = TPSImage('', landmarks=None, curves=[TPSCurve(TPSPoints(NArr([[0, 0]])))])

    min_arr0, max_arr0 = U.get_all_curve_extents(test_im0)
    np_test.assert_almost_equal(min_arr0, [0, 0])
    np_test.assert_almost_equal(max_arr0, [0, 0])

    test_im1 = TPSImage('', landmarks=None, curves=[TPSCurve(TPSPoints(NArr([[0, 0]]))),
                                                    TPSCurve(TPSPoints(NArr([[0, 0]])))])

    min_arr1, max_arr1 = U.get_all_curve_extents(test_im1)
    np_test.assert_almost_equal(min_arr1, [0, 0])
    np_test.assert_almost_equal(max_arr1, [0, 0])

    test_im2 = TPSImage('', landmarks=None, curves=[TPSCurve(TPSPoints(NArr([[0, 0],
                                                                             [1, 1]]))),
                                                    TPSCurve(TPSPoints(NArr([[0, 0]])))])

    min_arr2, max_arr2 = U.get_all_curve_extents(test_im2)
    np_test.assert_almost_equal(min_arr2, [0, 0])
    np_test.assert_almost_equal(max_arr2, [1, 1])

    test_im3 = TPSImage('', landmarks=None, curves=[TPSCurve(TPSPoints(NArr([[0, 0],
                                                                             [1, 1]]))),
                                                    TPSCurve(TPSPoints(NArr([[-1, -2],
                                                                             [0, 0]])))])

    min_arr3, max_arr3 = U.get_all_curve_extents(test_im3)
    np_test.assert_almost_equal(min_arr3, [-1, -2])
    np_test.assert_almost_equal(max_arr3, [1, 1])

    test_im4 = TPSImage('', landmarks=None, curves=[TPSCurve(TPSPoints(NArr([[-2, 0],
                                                                             [1, 1]]))),
                                                    TPSCurve(TPSPoints(NArr([[-1, -2],
                                                                             [0, 2]])))])

    min_arr4, max_arr4 = U.get_all_curve_extents(test_im4)
    np_test.assert_almost_equal(min_arr4, [-2, -2])
    np_test.assert_almost_equal(max_arr4, [1, 2])


def test_nearest_segment():
    # flat y seg, on
    dists, intersects = U.nearest_segment(NArr([0, 0]), NArr([[-1, 0], [1, 0]]))

    np_test.assert_almost_equal(dists, 0)
    np_test.assert_almost_equal(intersects, [0, 0])

    # flat x seg, on
    dists, intersects = U.nearest_segment(NArr([0, 0]), NArr([[0, -1], [0, 1]]))

    np_test.assert_almost_equal(dists, 0)
    np_test.assert_almost_equal(intersects, [0, 0])

    # flat y seg, not on
    dists, intersects = U.nearest_segment(NArr([0, 1]), NArr([[-1, 0], [1, 0]]))

    np_test.assert_almost_equal(dists, 1)
    np_test.assert_almost_equal(intersects, [0, 0])

    # flat x seg, not on
    dists, intersects = U.nearest_segment(NArr([1, 0]), NArr([[0, -1], [0, 1]]))

    np_test.assert_almost_equal(dists, 1)
    np_test.assert_almost_equal(intersects, [0, 0])

    # not flat seg, on
    dists, intersects = U.nearest_segment(NArr([0, 0]), NArr([[-1, -1], [1, 1]]))

    np_test.assert_almost_equal(dists, 0)
    np_test.assert_almost_equal(intersects, [0, 0])

    # not flat seg, not on
    dists, intersects = U.nearest_segment(NArr([0, 1]), NArr([[-1, -1], [1, 1]]))

    np_test.assert_almost_equal(dists, np.sqrt(2) / 2)
    np_test.assert_almost_equal(intersects, [0.5, 0.5])

    # multiple segs, nearest to first
    dists, intersects = U.nearest_segment(NArr([0, 1]), NArr([[-1, 0], [1, 0], [2, 0]]))

    np_test.assert_almost_equal(dists, 1)
    np_test.assert_almost_equal(intersects, [0, 0])

    # multiple segs, nearest to first
    dists, intersects = U.nearest_segment(NArr([-1, 1]), NArr([[-1, -1], [1, 1], [2, 1]]))

    np_test.assert_almost_equal(dists, np.sqrt(2))
    np_test.assert_almost_equal(intersects, [0, 0])

    dists,intersects = U.nearest_segment(NArr([1,1]), NArr([[0.5,1],[1.5, 1]]))
    np_test.assert_almost_equal(dists, 0)
    np_test.assert_almost_equal(intersects, [1, 1])

    # segment going in non increasing y
    dists,intersects = U.nearest_segment(NArr([1,1]), NArr([[1, 0.5],[1, 1.5]]))
    np_test.assert_almost_equal(dists, 0)
    np_test.assert_almost_equal(intersects, [1, 1])

    # works with shapely

    dists,intersects = U.nearest_segment(NArr([1,1]), LineString(NArr([[1, 0.5],[1, 1.5]])))
    np_test.assert_almost_equal(dists, 0)
    np_test.assert_almost_equal(intersects, [1, 1])

def test_nearest_segment_sides():

    result = U.nearest_segment(NArr([0, 0]), NArr([[-1, 0], [1, 0]]), on_side=U.Side.LEFT_SIDE)

    np_test.assert_almost_equal(result[0], 0)
    np_test.assert_almost_equal(result[1], [0, 0])

    result = U.nearest_segment(NArr([0, 0.5]), NArr([[-1, 0], [1, 0]]), on_side=U.Side.LEFT_SIDE)

    np_test.assert_almost_equal(result[0], 0.5)
    np_test.assert_almost_equal(result[1], [0, 0])

    result = U.nearest_segment(NArr([0, -0.5]), NArr([[-1, 0], [1, 0]]), on_side=U.Side.LEFT_SIDE)

    assert result is None

    result = U.nearest_segment(NArr([0, -0.5]), NArr([[-1, 0], [1, 0]]), on_side=U.Side.RIGHT_SIDE)

    np_test.assert_almost_equal(result[0], 0.5)
    np_test.assert_almost_equal(result[1], [0, 0])

    result = U.nearest_segment(NArr([0, -0.5]), NArr([[-1, 0], [1, 0]]), on_side=U.Side.RIGHT_SIDE, next_point=NArr([1,-0.5]))

    assert result is None
    result = U.nearest_segment(NArr([0, -0.5]), NArr([[-1, 0], [1, 0]]), on_side=U.Side.LEFT_SIDE, next_point=NArr([1,-0.5]))

    np_test.assert_almost_equal(result[0], 0.5)
    np_test.assert_almost_equal(result[1], [0, 0])

def test_get_all_curve_intersections():
    points_0 = NArr([[-1, 1]])
    curve_0 = NArr([[-1, -1], [1, 1]])
    dists0, intersects0 = U.get_all_curve_intersections(points_0, curve_0)

    np_test.assert_array_almost_equal(dists0, [np.sqrt(2)])
    np_test.assert_array_almost_equal(intersects0, [[0, 0]])

    points_1 = NArr([[-1, 1], [1.5, 2]])
    curve_1 = NArr([[-1, -1], [1, 1], [2, 1]])
    dists0, intersects0 = U.get_all_curve_intersections(points_1, curve_1)

    np_test.assert_array_almost_equal(dists0, [np.sqrt(2), 1])
    np_test.assert_array_almost_equal(intersects0, [[0, 0], [1.5, 1]])


def test_inside_curves():
    def _arr_sort_l(arr):
        return sorted(arr.tolist())
    # area one
    inside0 = U.inside_polygon(NArr([[0, 0], [0, 1], [1, 1], [1, 0]]))

    assert _arr_sort_l(inside0) == [[0,0],[0,1],[1,0],[1,1]]

    # area 1.5
    inside1 = U.inside_polygon(NArr([[0, 0], [0, 2],[1, 1],[1, 0]]))

    assert _arr_sort_l(inside1) == [[0,0],[0,1],[0,2],[1,0],[1,1]]

    # area 2
    inside2 = U.inside_polygon(NArr([[0, 0], [0, 2],[1, 2],[1, 0]]))

    assert _arr_sort_l(inside2) == [[0,0],[0,1],[0, 2], [1,0], [1,1], [1, 2]]

    # area 4
    inside3 = U.inside_polygon(NArr([[0, 0], [0, 2], [2, 2], [2, 0]]))
    assert _arr_sort_l(inside3) == [[0,0],[0,1],[0, 2], [1,0], [1,1], [1,2], [2,0],[2,1],[2,2]]

def test_distance_along():
    test_curve = NArr(([[0, 0], [0, 1]]))
    dist_0 = U.distance_along(test_curve, NArr([0, 0.0]))
    np_test.assert_almost_equal(dist_0, 0.0)

    test_curve = NArr(([[0,0],[0,1]]))
    dist_0 = U.distance_along(test_curve, NArr([0,0.5]))
    np_test.assert_almost_equal(dist_0, 0.5)

    test_curve = NArr(([[0,0],[0,1],[1,2]]))
    dist_1 = U.distance_along(test_curve, NArr([0.5,1.5]))
    np_test.assert_almost_equal(dist_1, 1+np.sqrt(2)/2)


def test_real_example_inside():
    c0=  NArr([[1872., 1876.],
               [1915., 1966.],
               [1958., 2055.]
               ])

    c1  = NArr([[1869., 1888.],
               [1871., 1973.],
               [1874., 2058.]])
    #inside_points, curve_0_inside, curve_1_inside = U.inside_curves(curve_0=c0, curve_1=c1)
    pass


def test_make_valid_side():
    side = U.make_valid_side(NArr([-10, 10]), NArr([0, 0]), NArr([10, 0]), NArr([20, -10]), U.Side.RIGHT_SIDE, use_size=200)
    assert side.intersects(Point(-10,0))
    assert side.intersects(Point(5, -5))
    assert side.intersects(Point(15, -7))

    assert not side.intersects(Point(-5, 10))
    assert not side.intersects(Point(5, 5))
    assert not side.intersects(Point(15, 7))


    side = U.make_valid_side(NArr([-10, 10]), NArr([0, 0]), NArr([10, 0]), NArr([20, -10]), U.Side.LEFT_SIDE, use_size=200)
    assert not side.intersects(Point(-10,5))
    assert not side.intersects(Point(5, -5))
    assert not side.intersects(Point(15, -7))

    assert side.intersects(Point(-5, 10))
    assert side.intersects(Point(5, 5))
    assert side.intersects(Point(15, 0))

    side = U.make_valid_side(NArr([-10, 0]), NArr([0, 10]), NArr([10, 10]), NArr([20, 0]), U.Side.RIGHT_SIDE, use_size=200)
    assert side.intersects(Point(-5,0))
    assert side.intersects(Point(5, 9))
    assert side.intersects(Point(15, 2))

    assert not side.intersects(Point(-5, 10))
    assert not side.intersects(Point(5, 15))
    assert not side.intersects(Point(15, 7))


def test_interpolate_image_from_coords():

    """
                    image: np.ndarray, original_inside_coords: np.ndarray,
                                  projected_bound_coords: np.ndarray, projected_inside_cords: np.ndarray,
                                  n_nearest: int = 9, verbose: bool = True,
                                  projection_radius_out=False)"""

    test_image = np.zeros((5,5,3), dtype=float)
    test_image[1:4,1:4] = 1

    original_inside_coords = np.asarray([[1,1],[1,2],[1,3],[2,1],[2,2],[2,3],[3,1],[3,2],[3,3]])
    projected_inside_coords = original_inside_coords+1

    projected_bound_coords = np.asarray([[2,2],[2,4],[4,4],[4,2], [2,2]])

    img, projection_out = U.interpolate_image_from_coords(test_image, original_inside_coords, projected_bound_coords,
                                                          projected_inside_coords,
                                                          projection_radius_out=True)

    #interp_value = ((norm.pdf(1)*np.asarray([2.0,4.0,6.0,8.0])).sum()+(norm.pdf(np.sqrt(2))*np.asarray([1.0,3.0,7.0,9.0])).sum()
    #                +norm.pdf(0)*np.sqrt(5))/(norm.pdf(1)*4+norm.pdf(np.sqrt(2))*4+norm.pdf(0))
    interp_dist = (norm.pdf(1)*4+norm.pdf(np.sqrt(2))*4*np.sqrt(2))/(norm.pdf(1)*4+norm.pdf(np.sqrt(2))*4+norm.pdf(0))
    np_test.assert_array_almost_equal(img[3,3], np.asarray([1,1,1]))
    np_test.assert_array_almost_equal(projection_out[3,3], np.asarray([[interp_dist]]))

def make_base():
    # Simple striped rectangle

    width = 5
    height = 11
    n_reps = 16
    img = np.tile(np.concatenate([np.zeros((height, width, 3)), np.ones((height, width, 3))], axis=1), (1, n_reps, 1))
    img = np.concatenate([img, np.zeros((height, 1, 3))], axis=1)
    img_height, img_width = img.shape[:2]
    c1 = np.asarray([[0, c] for c in range(0, img_width, width * 2)])
    c2 = np.asarray([[height - 1, c] for c in range(0, img_width, width * 2)])
    return img, c1,c2

def test_examples():
    # Simple striped rectangle
    img, c1,c2 = make_base()
    # Control curves.
    img_height, img_width = img.shape[:2]

    angle = np.pi/2
    rot90 = np.asanyarray([[np.cos(angle), -np.sin(angle)],[np.sin(angle),np.cos(angle)]])
    #c190 = rot90.as_rotvec()#*c1
    c190 = np.dot(c1,rot90)+[0,img_height-1]
    c290 = np.dot(c2,rot90)+[0,img_height-1]

    cv2rot90 = cv2.getRotationMatrix2D(((img.shape[1]-1)/2,(img.shape[1]-1)/2),90,1)
    img90 = cv2.warpAffine(img,cv2rot90, (img.shape[0],img.shape[1]),1,cv2.INTER_NEAREST)
    img_new, c2_new,c1_new,proj_radius = U.reproject_snakelike(c290, c190, img90, projection_radius_out=True, verbose=True, n_nearest=1)
    assert np.median(proj_radius)<1
    assert np.max(proj_radius) < 1.6
    assert img_new.shape == img.shape


def bend_coords(row, height, bends, theta, ratio, bend_exp, ends_only=False):
    cur_angle = 0
    cur_theta = theta
    cur_row_start = row
    if ends_only:
        n_coords = bends.shape[0]
    else:
        n_coords = bends[-1]
    new_coords = np.zeros((n_coords, 2))
    if ends_only:
        orig_coords = bends
    else:
        orig_coords = np.concatenate([np.full((n_coords, 1), row), np.arange(n_coords).reshape(-1, 1)], axis=1)
    bend_diff = np.diff(bends)
    cur_stretch = height / np.tan(np.deg2rad((90 - cur_theta * 0.5))) * (ratio - 0.5)

    cur_col_start = -(cur_stretch) / 2

    # new_lengths= bend_diff + scaling
    cur_sample = 0
    for offset in range(len(bend_diff)):
        n_samples = bend_diff[offset]
        cur_stretch = height / np.tan(np.deg2rad((90 - cur_theta * 0.5))) * (ratio - 0.5) * 2
        prev_theta = theta * (bend_exp ** (offset - 1))
        prev_stretch = height / np.tan(np.deg2rad((90 - prev_theta * 0.5))) * (ratio - 0.5) * 2

        # next_stretch =height/np.tan(np.deg2rad((90-cur_theta*0.5))) * (ratio-0.5)*2
        new_length = n_samples + cur_stretch + (cur_stretch - prev_stretch) / 2

        cur_ang_rad = np.deg2rad(cur_angle)
        rot_trans = np.asarray([[np.cos(cur_ang_rad), -np.sin(cur_ang_rad), cur_row_start],
                                [np.sin(cur_ang_rad), np.cos(cur_ang_rad), cur_col_start],
                                [0, 0, 1]])
        length_vec = np.asarray([[0], [new_length], [1]])
        new_end = rot_trans.dot(length_vec)[:2].flatten()
        if ends_only:
            new_coords[offset, :] = [cur_row_start, cur_col_start]
        else:
            if np.isclose(cur_row_start, new_end[0]):
                new_row_samp = np.ones((n_samples,)) * cur_row_start
            else:
                new_row_samp = np.arange(cur_row_start, new_end[0], (new_end[0] - cur_row_start) / n_samples)

            if np.isclose(cur_col_start, new_end[1]):
                new_col_samp = np.ones((n_samples,)) * cur_col_start
            else:
                new_col_samp = np.arange(cur_col_start, new_end[1], (new_end[1] - cur_col_start) / n_samples)
            new_end_sample = cur_sample + n_samples

            # print(cur_sample, new_end_sample, new_row_samp)
            new_coords[cur_sample:new_end_sample, 0] = new_row_samp
            new_coords[cur_sample:new_end_sample, 1] = new_col_samp
            cur_sample = new_end_sample

        cur_row_start = new_end[0]
        cur_col_start = new_end[1]
        cur_theta = theta * (bend_exp ** (offset + 1))
        cur_angle += cur_theta
    if ends_only:
        if ends_only:
            new_coords[-1, :] = new_end

    return new_coords, orig_coords




def test_bend():
    img, c1, c2 = make_base()
    # Control curves.
    img_height, img_width = img.shape[:2]

    all_new_coords = []
    orig_coords = []
    for row in range(img_height):
        new, orig = bend_coords(row, img_height, c1[:, 1], 12, row / (img_height - 1), 1)
        all_new_coords.append(new)
        orig_coords.append(orig)

    c1_new_bend, _ = bend_coords(0, img_height, c1[:, 1], 12, 0, 1, ends_only=True)
    c2_new_bend, _ = bend_coords(img_height - 1, img_height, c1[:, 1], 12, 1, 1, ends_only=True)
    all_new_coords = np.concatenate(all_new_coords, axis=0)
    all_min = c2_new_bend.min(axis=0)
    all_new_coords = all_new_coords - all_min
    c1_new_bend = c1_new_bend - all_min
    c2_new_bend = c2_new_bend - all_min
    orig_coords = np.concatenate(orig_coords, axis=0)

    inside_spiral = U.inside_polygon(np.concatenate([c1_new_bend, np.flipud(c2_new_bend)], axis=0))
    coords_search = cKDTree(all_new_coords)
    nearest_distances, nearest_indexes = coords_search.query(inside_spiral, 1, n_jobs=-1)
    spiralmax = c2_new_bend.max(axis=0)

    spiral_proj = np.zeros((int(spiralmax[0]) + 2, int(spiralmax[1]) + 2, 3))
    for ind in range(len(inside_spiral)):
        nn = nearest_indexes[ind]
        spiralloc = np.round(inside_spiral[ind]).astype(int)
        flatloc = orig_coords[nn]
        spiral_proj[spiralloc[0], spiralloc[1], :] = img[flatloc[0], flatloc[1], :]

    img_reproj_spir, c2_spir_new,c1_spir_new, proj_err = U.reproject_snakelike(c2_new_bend, c1_new_bend, spiral_proj,
                                                                               projection_radius_out=True,
                                                                               verbose=False, n_nearest=1)

    # came out at 13/160
    eq = img_reproj_spir[1:-1, :, 0] == img[:, :img_reproj_spir.shape[1], 0]

    assert eq.mean() > 0.97

    assert proj_err.max() < 1

if __name__ == '__main__':
    test_examples()
