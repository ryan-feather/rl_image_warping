__author__ = "Ryan Feather"
# given a reference set of curves
#  choices - "straight tube" = two parallel long lines
#          - refernce straight snake
# given a set of curves (from landmarked image)
# create 1:1 correspondence between reference curves and target curves
# define a region that is "inside" target curves, namely
#    x,y E target set iff. |(x,y) - curve_0| <  |curve_0 - curve_1|  & |(x,y) - curve_1| < |curve_0 - curve_1|
#          & min(|(x,y) - curve_0|, |(x,y) - curve_1|) < |(x,y) - image_bound|
# for each pixel in the region, define new set of coordinates
#  d_c0 = distance to curve 0
#  d_c1 = distance to curve_1
#  l_c0 = length along curve 0 to nearest point
#  l_c1 = length along curve 1 to nearest point
#  c0_c1 =|curve_0 - curve_1|
#  map r[x,y] to I[x,y] in new coordinates space  (d_c0-d_c1)/2-(d_c0/2+d_c1/2),(l_c0+l_c1)/2
# interpolate pixel values using
#   - nearest neighbor
#   - thin plate splines
#   - bicubic interpolation
# also, directly copy certain segments of the base images to the output for purposes of calibration patches
usage = """

unspiral_sankelike.py base_tps image_tps image_directory output_directory

Warps each image with curves described in image_tps to fit within the reference curves described in base_tps. Images
are expected to be in image_directory and output_directory. Outputs images are sized to contain at a minimum the extents
of the reference curves and any direct transfer blocks (see below).  If output_directory does not exist, it will be
created.

Optionally, extend with --transfer START_ROW START_COL END_ROW END_COL to directly transfer the block of pixel values
between  (inclusive) the given bounds from each image to the resulting output images images.
"""
import argparse
import logging
import os
import sys

import numpy as np
import skimage.io

from py_tps.tps_io import TPSFile, TPSImage, TPSCurve, TPSPoints
from rl_image_warping import image_warping


def error_out(message: str):
    """ Helper that prints the message to stderr and exits with an error (non-zero) exit code.

    :param message:
    :return:
    """
    print(message, file=sys.stderr)
    exit(1)


def create_final_image(reprojected: np.ndarray, original: np.ndarray) -> np.ndarray:
    """ Put together final output image.

    :param reprojected:
    :param original:
    :return: original image with reprojected stacked on top. Original or snake will be right padded with 0s to match
    """
    if reprojected.shape[1] > original.shape[1]:
        pad_width = reprojected.shape[1] - original.shape[1]
        original_padded = np.pad(original, [(0, 0), (0, pad_width), (0, 0)], mode='constant')
        composite = np.vstack((reprojected, original_padded))
    else:
        pad_width = original.shape[1] - reprojected.shape[1]
        reprojected_padded = np.pad(reprojected, [(0, 0), (0, pad_width), (0, 0)], mode='constant')
        composite = np.vstack((reprojected_padded, original))
    return composite


def warp_images(arguments, output_file_type='png'):
    """  Main function that iterates through images and performs the image warping.  Provides some printed
    output to help track progress.

    :param arguments: argparse output. See help description from main block.
    :return: None
    """

    image_tps = TPSFile.read_file(arguments.image_tps)
    n_images = len(image_tps.images)

    if not os.path.exists(arguments.output_directory):
        os.makedirs(arguments.output_directory)

    result_logger = logging.getLogger() # get the root logger
    log_handler = logging.FileHandler(os.path.join(arguments.output_directory, 'errors.log'))
    log_handler.setLevel(logging.INFO)
    result_logger.addHandler(log_handler)
    print(f'Found {n_images} images in {arguments.image_tps}.')
    output_images = []

    def _flip_curve(curve, img):
        curve_new = curve[:, [1, 0]]
        curve_new[:, 0] = img.shape[0] - curve_new[:, 0] - 1
        return curve_new

    for image_ind in range(0, len(image_tps.images)):

        try:
            image = image_tps.images[image_ind]
            print(f'Processing image {image_ind+1}/{n_images}: {image.image}.')

            if image.curves is None or len(image.curves) == 0:
                error_out(f'No CURVES found in reference TPS file {arguments.image_tps}[{image.image}].')

            input_image_file = os.path.join(arguments.image_directory, image.image)
            if not os.path.isfile(input_image_file):
                error_out(f'{input_image_file} not found. Check that image exists in given directory.')

            curve_0 = image.curves[0]
            curve_1 = image.curves[1]
            print(f'Reading {input_image_file}')
            image_data = skimage.io.imread(input_image_file)
            # flip x coord and axis since these are from TPSDig

            curve_0 = _flip_curve(curve_0.tps_points.points, image_data)
            curve_1 = _flip_curve(curve_1.tps_points.points, image_data)
            #print(LineString(curve_0).length,LineString(curve_1).length, image_data.shape) #helpful in debugging

            reprojection_results = image_warping.reproject_snakelike(curve_0, curve_1, image_data, verbose=True,
                                                             auto_deoverlap=True,
                                                             projection_radius_out=arguments.get_projection_error)
            reprojected, new_coords_outer, new_coords_inner = reprojection_results[:3]
            composite = create_final_image(reprojected, image_data)
            # save as a bmp for lossless but big file
            new_image_name = f'{image.image}.{output_file_type}'
            output_image_file = os.path.join(arguments.output_directory, new_image_name)
            skimage.io.imsave(output_image_file, composite)
            curves = [TPSCurve(TPSPoints(new_coords_outer)), TPSCurve(TPSPoints(new_coords_inner))]
            new_tps_image = TPSImage(new_image_name, landmarks=None, curves=curves,
                                     id_number=image.id_number, comment=image.comment, scale=image.scale)
            output_images.append(new_tps_image)

            if arguments.get_projection_error:
                projection_error = reprojection_results[3]
                np.save(os.path.join(arguments.output_directory,image.image), projection_error)

        except image_warping.DeoverlapError as deo:
            logging.error(f'Failed to deoverlap image {image.image}: {deo.args[0]}')
        except Exception:
            logging.exception(f'Encountered unknown exception on {image.image}. Aborting.')
            raise
        # create curves, add to image
        # catch exception, put in log
    output_image = TPSFile(output_images)
    output_image.write_to_file(arguments.output_tps)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Reference line based image warper.", usage=usage)
    parser.add_argument('image_tps', type=str, help='TPS file containing curves for each target image.')
    parser.add_argument('image_directory', type=str, help='Base directory for input image files.')
    parser.add_argument('output_directory', type=str, help="Base directory for output image files. "
                                                           "If it doesn't exist, it will be created.")
    parser.add_argument('output_tps', type=str, help="New TPS file with results.")
    parser.add_argument('--get_projection_error', action='store_true', default=False,
                        help="If flag is set, will store weighted average projection interpolation distance as a "
                             "numpy array in image_directory")

    arguments = parser.parse_args()

    bad_file_message = ' is not a file. Please check that the specified path is correct.'

    if not os.path.isfile(arguments.image_tps):
        error_out(f'{arguments.image_tps}{bad_file_message}')

    if not os.path.isdir(arguments.image_directory):
        error_out(
            f'Image directory {arguments.image_directory} is not a directory. Please check that the specified path is correct.')

    if os.path.isfile(arguments.output_directory):
        error_out(
            f'Output directory {arguments.output_directory} is a file, not a directory. Check the help by using -h.')

    if not os.path.exists(arguments.output_directory):
        print(f'Output directory {arguments.output_directory} does not exist. Creating directory.')
        os.makedirs(arguments.output_directory)

    warp_images(arguments)
