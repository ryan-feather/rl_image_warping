from enum import Enum
from typing import Tuple, Union

import numpy as np
from numpy import linalg
from pyprind import ProgPercent
from scipy.spatial import cKDTree
from scipy.stats import norm
from shapely.geometry import Point, Polygon, MultiPoint, LineString
from shapely.geometry.base import BaseGeometry
from shapely.ops import nearest_points

from py_tps.tps_io import TPSImage


def verb_print(verbose: bool, *args):
    """ Helper to print if in verbose mode.

    :param verbose:
    :param args: print args
    :return:
    """
    if verbose:
        print(*args)


class VerbProgPercent(ProgPercent):
    # helper progpercent with verbosity flag
    def __init__(self, verbose, *args, **kwargs):
        self._verbose = verbose
        super(VerbProgPercent, self).__init__(*args, **kwargs)

    def update(self, iterations=1, item_id=None, force_flush=False):
        if self._verbose:
            super(VerbProgPercent, self).update(iterations=iterations, item_id=item_id, force_flush=force_flush)


def typed_evaluate(item, test_type, action):
    # helper for smart castings
    if isinstance(item, test_type):
        return action(item)
    else:
        return item


def euclidean(a, b):
    """ Simple helper for L2 distance for readability

    :param a:
    :param b:
    :return: Euclidean distance between point(s) a and point(s) b
    """

    return np.sqrt(np.sum((a - b) ** 2, axis=1))


def get_all_curve_extents(tps_image: TPSImage) -> Tuple[np.ndarray, np.ndarray]:
    """ The the bounds of the image

    :param tps_image:  TPSImage
    :return: min coordinate, max coordinate
    """
    all_curve_points = np.vstack((curve.tps_points.points for curve in tps_image.curves))

    min_coords = all_curve_points.min(axis=0)
    max_coords = all_curve_points.max(axis=0)
    return min_coords, max_coords


ROTATION = np.array([[0, -1],
                     [1, 0]])  # pi/2 rotation
MAX_POSSIBLE_SIZE = 100000  # how many pixels could we ever really have? This should probably be determined by the value automatically,


# but there doesnt seem to be a computational problem with going with the max here


class Side(str, Enum):
    #: Flag describing which side to look on
    RIGHT_SIDE = 'right'  #: between angle -pi and 0 of a vector
    LEFT_SIDE = 'left'  #: between angle 0 and pi of a vector


def other_side(side: Side):
    if side == Side.RIGHT_SIDE:
        return Side.LEFT_SIDE
    else:
        return side.RIGHT_SIDE


def nearest_segment(point: np.ndarray, segments: Union[np.ndarray, LineString], on_side: Side = None,
                    next_point: np.ndarray = None, next_point2: np.ndarray = None, prev_point: np.ndarray = None) -> \
Union[Tuple[
          float, np.ndarray], None]:
    """ Find the nearest segment index, distance to that segment, and nearest point on that
    segment to the point.

    :param point: x,y coordinates (2,)
    :param segments: x,y coordinates of connected line segments (n,2)
    :param on_side: if provided, is one of
    :return:  nearest segment index, distance to that segment, and nearest point on that
    segment to the point
    """

    shapely_point = Point(point)
    segments_string = typed_evaluate(segments, np.ndarray, LineString)

    if on_side is not None and next_point is not None:
        # create a plane containing 1 side of the line at the given angle

        valid_poly = make_valid_side(prev_point, point, next_point, next_point2, on_side)
        intersected_segment = segments_string.intersection(valid_poly)
        if intersected_segment.is_empty:  # point is not on right side
            return None
        else:
            return _nearest_on_geometry(intersected_segment, shapely_point)

    elif on_side is not None and next_point is None:
        n_segments = len(segments_string.coords) - 1
        nearest_dist = np.full((n_segments,), np.nan)
        nearest_intersection = np.full((n_segments, 2), np.nan)
        for segment_ind in range(n_segments):
            point = np.asarray(segments_string.coords[segment_ind])
            next_point = np.asarray(segments_string.coords[segment_ind + 1])
            if segment_ind > 0:
                prev_point = np.asarray(segments_string.coords[segment_ind - 1])
            else:
                prev_point = None
            if segment_ind < n_segments - 1:
                next_point2 = np.asarray(segments_string.coords[segment_ind + 2])
            else:
                next_point2 = None
            valid_poly = make_valid_side(prev_point, point, next_point, next_point2, on_side)

            if shapely_point.intersects(valid_poly):
                segment = LineString(segments_string.coords[segment_ind:segment_ind + 2])
                nearest_result = _nearest_on_geometry(segment, shapely_point)
                if nearest_result is not None:
                    nearest_dist[segment_ind], nearest_intersection[segment_ind, :] = nearest_result
        valid_intersections = np.isfinite(nearest_dist)
        if valid_intersections.any():
            valid_nearest_dist = nearest_dist[valid_intersections]
            valid_intersections = nearest_intersection[valid_intersections, :]
            nearest = np.argmin(valid_nearest_dist)
            return valid_nearest_dist[nearest], valid_intersections[nearest, :]
        else:
            return None
    else:

        return _nearest_on_geometry(segments_string, shapely_point)


def make_valid_side(prev_point, point, next_point, next_point2, on_side, use_size: float = None):
    valid_poly, invalid_poly = make_valid_and_anti_region(point, next_point, on_side, use_size=use_size)
    as_vector = (next_point - point).reshape(1, -1)
    # further constrain if needed
    valid_poly_copy = Polygon(valid_poly)
    valid_poly_copy2 = Polygon(valid_poly)

    if prev_point is not None:
        prev_region, invalid_prev_region = make_valid_and_anti_region(prev_point, point, on_side, use_size=use_size)
        prev_vector = (point - prev_point).reshape(1, -1)
        if on_side == Side.LEFT_SIDE:
            prev_angle = get_vector_angles(as_vector, prev_vector)
        else:
            prev_angle = get_vector_angles(as_vector, -prev_vector)

        if prev_angle > np.pi:
            valid_poly_copy = valid_poly_copy.difference(invalid_prev_region)
            # prev_region = Polygon([point, valid_poly.exterior.coords[3], prev_region.exterior.coords[3], prev_region.exterior.coords[4]])
            # valid_poly_copy = valid_poly_copy.difference(invalid_prev_region).union(prev_region).union(valid_poly_copy)
            # else:

            # prev_region = Polygon([prev_point, point, valid_poly.exterior.coords[3], prev_region.exterior.coords[3], prev_region.exterior.coords[4]])
            # valid_poly_copy = valid_poly_copy.union(prev_region).difference(invalid_prev_region).difference(invalid_poly)
            # to_subtract = valid_poly.union(prev_region).difference(valid_poly_copy)
            # valid_poly_copy = valid_poly
    if next_point2 is not None:
        next_region, invalid_next_region = make_valid_and_anti_region(next_point, next_point2, on_side,
                                                                      use_size=use_size)
        next_vector = (next_point2 - next_point).reshape(1, -1)
        if on_side == Side.LEFT_SIDE:
            next_angle = get_vector_angles(as_vector, next_vector)
        else:
            next_angle = get_vector_angles(as_vector, -next_vector)

        if next_angle < np.pi:
            valid_poly_copy = valid_poly_copy.difference(invalid_next_region)
        # valid_poly_copy2 = valid_poly_copy2.difference(invalid_next_region).union(next_region).union(valid_poly_copy2)
        # else:
        #    valid_poly_copy2 = valid_poly_copy2.union(next_region).difference(invalid_next_region).difference(invalid_poly)
        to_subtract2 = valid_poly.union(next_region).difference(valid_poly_copy2)
    return valid_poly_copy  # valid_poly_copy.union(valid_poly_copy2)#.difference(to_subtract)#.difference(to_subtract2)


def _nearest_on_geometry(geometry: BaseGeometry, shapely_point: Point) -> Union[Tuple[float, np.ndarray], None]:
    """

    :param geometry:
    :param shapely_point:
    :return: distance to geometry of point, projection (nearest point on) geometry from point
    """
    # want to get nearest point and the segment it's in
    _, intersection = nearest_points(shapely_point, geometry)
    if intersection is not None:
        distance = shapely_point.distance(intersection)
        return distance, np.asarray(intersection.xy)[:, 0]  # comes as a 2x1
    else:
        return None


def make_valid_region(point: np.ndarray, next_point: np.ndarray, on_side: Side, use_size: float = None) -> Polygon:
    """ Make a polygon representing one side of a vector

    :param point: start point of vector
    :param next_point: end point of vector
    :param on_side:
    :return: valid rectangle
    """

    side_size = use_size if use_size is not None else MAX_POSSIBLE_SIZE
    if np.isclose(next_point, point).all():
        raise ValueError('next_point and point are the same. Cannot calculate slope.')
    # make a big quadrilateral with one corner on the point containing the angle at_angle
    slope = next_point - point
    slope = slope / np.linalg.norm(slope)  # normal for easy multiplying
    orthogonal_slope = slope @ ROTATION
    half_max = side_size / 2
    start_point = (point + next_point) / 2
    if on_side == Side.LEFT_SIDE:
        point0 = start_point + half_max * -slope
        point1 = point0 + side_size * -orthogonal_slope
        point2 = point1 + side_size * slope
        point3 = point2 + side_size * orthogonal_slope
    else:  # just flip your slope signs!
        point0 = start_point + half_max * slope
        point1 = point0 + side_size * orthogonal_slope
        point2 = point1 + side_size * -slope
        point3 = point2 + side_size * -orthogonal_slope
    ok_poly = Polygon([point0, point1, point2, point3])
    return ok_poly


def make_valid_and_anti_region(point: np.ndarray, next_point: np.ndarray, on_side: Side, use_size: float = None) -> \
Tuple[Polygon, Polygon]:
    valid = make_valid_region(point, next_point, on_side, use_size=use_size)
    invalid = make_valid_region(next_point, point, on_side, use_size=use_size)
    return valid, invalid


class NoIntersectionFound(Exception):
    pass


class InvalidCurvesException(Exception):
    pass


class DeoverlapError(Exception):
    pass


def get_angle_to_intersections(intersections: np.ndarray, point: np.ndarray, next_point: np.ndarray = None,
                               next_intersection_points: np.ndarray = None) -> np.ndarray:
    """ Get angle to each of the intersections.  If next_point is provided, the angle is based on the
    vectors point->next_point, point->intersections.  If next_intersection_points is instead provided, the
    angle is based on the vectors point->intersections, intersections->next_intersection_points

    :param intersections: intersections of point on some curve
    :param point: point for getting angles
    :param next_point: next point on the same curve as point
    :param next_intersection_points: next point after intersections on the curve
    :return: angle vector between 0 and 2pi
    """

    if next_point is None and next_intersection_points is None:
        raise NotImplementedError('One of next_point or next_intersection_points must be provided.')
    elif next_point is not None and next_intersection_points is not None:
        raise NotImplementedError('Only one of next_point or next_intersection_points must be provided.')

    if next_point is not None:
        vector_from = (next_point - point).reshape(1, -1)  # (segments[1:, :] - intersections)
    elif next_intersection_points is not None:
        vector_from = next_intersection_points - intersections

    vectors_to = intersections - point
    return get_vector_angles(vector_from, vectors_to)


def get_vector_angles(vector_from, vectors_to):
    """

    :param vector_from:
    :param vectors_to:
    :return:
    """
    # want to get cos(θ) = (u.v)/(||u|*||v||) where u and v are vectors_from and vectors_to
    dot_product = (vector_from * vectors_to).sum(axis=1)
    vectors_from_norm = linalg.norm(vector_from, axis=1)
    vectors_to_norm = linalg.norm(vectors_to, axis=1)
    cos_theta = dot_product / (vectors_from_norm * vectors_to_norm)
    theta = np.arccos(cos_theta)
    # print('vfrom',vectors_from)
    sin_theta = np.cross(vector_from, vectors_to) / (vectors_from_norm * vectors_to_norm)
    sin = np.arcsin(sin_theta)
    theta[sin < 0] = 2 * np.pi - theta[sin < 0]
    return theta


def get_all_curve_intersections(points: np.ndarray, curve: np.ndarray, on_side: Side = None, verbose: bool = False) -> \
        Tuple[
            np.ndarray, np.ndarray]:
    """ Helper to loop over many points and get nearest_segment outputs

    :param points: array, (n,2)
    :param curve: array of connected segments, (m, 2)
    :param on_side: if given, indicates side that results must be on
    :param verbose: bool, print updates?
    :return: nearest distance from each point (n,) intersections (n,2)
    """
    n_points = points.shape[0]
    curve_distances = np.zeros((n_points,))
    intersections = np.zeros((n_points, 2), dtype=np.float64)
    verb_print(verbose, 'Get all curve intersections')
    prog_perc = VerbProgPercent(verbose, n_points)
    # at_angle = None # might initalie with angle

    curve = typed_evaluate(curve, np.ndarray, LineString)
    for point_ind in range(n_points):
        point = points[point_ind]
        next_point = None
        prev_point = None
        next_point2 = None
        if on_side is not None:

            if point_ind < n_points - 1:
                if point_ind < n_points - 2:
                    next_point2 = points[point_ind + 2]
                next_point = points[point_ind + 1]
                if point_ind > 0:
                    prev_point = points[point_ind - 1]
            else:
                # go back , flip sides

                next_point = point
                point = points[point_ind - 1]
                if point_ind > 1:
                    next_point2 = points[point_ind - 2]
                if on_side == Side.LEFT_SIDE:
                    on_side = Side.RIGHT_SIDE
                else:
                    on_side = Side.LEFT_SIDE

        result = nearest_segment(point, curve, on_side=on_side, next_point=next_point, prev_point=prev_point,
                                 next_point2=next_point2)
        # if point_ind==0 and maintain_orientation:
        #    at_angle = get_angle_to_intersections(result[2].reshape(1, -1), point, curve[result[0]:result[0]+2,:])

        if result is None and 0 < point_ind < n_points - 1:
            raise NoIntersectionFound(f"No intersection found for points[{point_ind}]")
        elif result is None: # force check last segment for end point
            shapely_point = Point(point)
            as_line_string = typed_evaluate(curve, np.ndarray, LineString)
            if point_ind == 0:
                segment = as_line_string.coords[2:]
            else:
                segment = as_line_string.coords[-2:]
            curve_distances[point_ind], intersections[point_ind] = _nearest_on_geometry(LineString(segment),
                                                                                        shapely_point)
        else:
            curve_distances[point_ind], intersections[point_ind] = result

        prog_perc.update()

    return curve_distances, intersections


def distance_along(curve: Union[np.ndarray, LineString], point: np.ndarray) -> float:
    """ Give the distance along the curve the given point is. Segment gives the exact segment the point is on
    since that is already known.

    :param curve:
    :param point:
    :return:
    """

    as_line = typed_evaluate(curve, np.ndarray, LineString)
    as_point = Point(point.flat)
    return as_line.project(as_point)


def get_all_distances_along(points: np.ndarray, curve: np.ndarray, verbose: bool = False) -> np.ndarray:
    """ Helper to loop over many points and get distances along the curve

    :param points: array, (n,2) points to check
    :param curve: array of connected segments, (m, 2)
    :param verbose: print some more output?
    :return: distances along the segment (n,)
    """

    n_points = points.shape[0]
    curve_distances = np.zeros((n_points,))
    verb_print(verbose, 'Get all distances along')
    prog_perc = VerbProgPercent(verbose, n_points)

    curve = typed_evaluate(curve, np.ndarray, LineString)
    for point_ind in range(n_points):
        curve_distances[point_ind] = distance_along(curve, points[point_ind].reshape(1, -1))
        prog_perc.update()
    return curve_distances


def inside_polygon(polygon: np.ndarray) -> np.ndarray:
    """ Determine all of the pixel coordinates inside a closed loop

    :param polygon: x,y array of coordinates forming a polygon
    :return: points inside (n,2)
    """

    min_extent = np.min(polygon, axis=0)
    max_extent = np.max(polygon, axis=0)
    as_shapely_poly = Polygon(polygon)
    # get a grid of all points between the curve extents so we can check constraints on them
    all_points_y, all_points_x = np.meshgrid(np.arange(min_extent[1], max_extent[1] + 1),
                                             np.arange(min_extent[0], max_extent[0] + 1))

    all_points_long = np.hstack((all_points_x.reshape(-1, 1), all_points_y.reshape(-1, 1))).astype(int)
    as_shapely_points = MultiPoint(all_points_long)
    inside_shapely = as_shapely_poly.intersection(as_shapely_points)
    intersection_coords = np.asarray([[o.xy[0][0], o.xy[1][0]] for o in inside_shapely.geoms]).astype(np.int)
    return intersection_coords


def reproject_snakelike(curve_0:  np.ndarray, curve_1: np.ndarray, image: np.ndarray, n_nearest: int = 9,
                        verbose: bool = False, auto_deoverlap: bool = True, projection_radius_out=False) -> \
        Union[Tuple[np.ndarray, np.ndarray, np.ndarray], Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]]:
    """ Reproject the points within the curves specified using the method:
        1) Project snake into a new coordinate space determined by length along the snake and distance from the sides
        2) For each pixel within snakes boundaries in new coordinate space, find the nearest projected pixels
            2.a) Average together values associated with the nearest pixels using Gaussian weights centered on the
                 current pixel

        The output curves may be flipped so that the order is outer, inner

    :param curve_0: defines one edge. Note that if these curves came from TPS Dig, the x coordinates should be flipped by
                    subtracting them from the image shape[0] beforehand
    :param curve_1: defines another edge
    :param image: image array (lengthxwidthxdepth)
    :param n_nearest: nearest neighbors count to use in interpolating results, default 9 which  in the optimal case
                      gives all of the surrounding pixel
    :param verbose: if true, print some extra output
    :param auto_deoverlap: if True, try to automatically find any overlaps in the two curves and fix
    :param projection_radius_out: if True, give back the reprojection weighted error distance matrix as well.
    :return: new image with no border containing the interpolated values, new coordinates c0, new coordinates c1
    """

    c0_shapley = LineString(curve_0)
    c1_shapley = LineString(curve_1)
    if c0_shapley.length > c1_shapley.length:
        outer_curve = curve_0
        inner_curve = curve_1
        inward_direction = Side.LEFT_SIDE
    else:
        outer_curve = curve_1
        inner_curve = curve_0
        inward_direction = Side.RIGHT_SIDE
    if auto_deoverlap:
        deoverlap_direction = other_side(inward_direction)
        inner_curve_deoverlap = deoverlap_curves(outer_curve, inner_curve, deoverlap_direction)
    else:
        inner_curve_deoverlap = inner_curve

    interpolator = InternalCurveInterpolator(curve_0=outer_curve, curve_1=inner_curve_deoverlap, num_lines=10,
                                             inward_direction=inward_direction)
    new_coords_outer, _, _ = interpolator.calculate_projection(outer_curve, verbose=verbose)
    new_coords_inner, _, _ = interpolator.calculate_projection(inner_curve, verbose=verbose)
    bound_points, bound_coords, _, _ = interpolator.all_boundary_points_coordinates(verbose=verbose)

    inside_points, all_coords, m_d, d_along = interpolator.all_ratio_line_coordinates(verbose=verbose)
    bottom_offset = bound_coords[:, 0].min()  # will start at some negative value
    all_coords[:, 0] -= bottom_offset
    bound_coords[:, 0] -= bottom_offset
    new_coords_outer[:, 0] -= bottom_offset
    new_coords_inner[:, 0] -= bottom_offset
    interpolation_result = interpolate_image_from_coords(image, inside_points, bound_coords, all_coords,
                                                         n_nearest=n_nearest, verbose=verbose,
                                                         projection_radius_out=projection_radius_out)

    if projection_radius_out:
        return interpolation_result[0], new_coords_outer, new_coords_inner, interpolation_result[1]
    else:
        return interpolation_result, new_coords_outer, new_coords_inner


def interpolate_image_from_coords(image: np.ndarray, original_inside_coords: np.ndarray,
                                  projected_bound_coords: np.ndarray, projected_inside_coords: np.ndarray,
                                  n_nearest: int = 9, verbose: bool = True,
                                  projection_radius_out=False) -> Tuple[np.ndarray]:
    """

    :param image: image array (lengthxwidthxdepth)
    :param original_inside_coords:
    :param projected_bound_coords:
    :param projected_inside_coords: all projected coordinates from the original
    :param n_nearest: nearest neighbors count to use in interpolating results, default 9 which  in the optimal case
                      gives all of the surrounding pixel
    :param verbose: if true, print some extra output
    :param projection_radius_out: if True, return also an array of the average weighted
                             distance projection information came from for each new pixel
    :return: new image, [projection radius out, projection raidius in]
    """
    if original_inside_coords.shape[0] < 9:
        raise ValueError('Expecting at least 9 coordinates')

    if original_inside_coords.shape[0] != projected_inside_coords.shape[0]:
        raise ValueError('Expecting same number of original and projected coordinates.')

    verb_print(verbose, 'Determining inside coordinates in projected space.')
    # now, get inside points of bound coords in new space
    inside_bounds = inside_polygon(projected_bound_coords)
    # build tree to do nearest neighbors in logn instead of n (will make whole operation n logn instead of n**2)
    verb_print(verbose, 'Finding nearest neighbors.')
    coords_search = cKDTree(projected_inside_coords)
    # then go through and do an nd neighbor search and interpolate
    nearest_distances, nearest_indexes = coords_search.query(inside_bounds, n_nearest, n_jobs=-1)
    if n_nearest == 1: # otherwise shape is wrong
        nearest_indexes = nearest_indexes.reshape((nearest_indexes.shape[0], 1))
        nearest_distances = nearest_distances.reshape((nearest_distances.shape[0], 1))
    distance_weights = norm.pdf(nearest_distances)
    rounded = inside_bounds.round().astype(np.uint64)
    new_image_size = (int(np.max(rounded[:, 0]) + 1), int(np.max(rounded[:, 1]) + 1), image.shape[2],)
    new_image = np.zeros(new_image_size)
    if projection_radius_out:
        projection_error_out = np.zeros(new_image_size[:2])
    # now go through each coord and
    # get the original pixel value at each coord corresponding to the ind
    # multiply by weight, sum, and put in new coord location on output image
    # flip coordsf
    verb_print(verbose, 'Building image.')
    float_img = image.astype(float)  # float for adding together
    for coord_ind in range(rounded.shape[0]):
        neighbor_coords = original_inside_coords[nearest_indexes[coord_ind, :], :]  # now go back to the original image
        new_coord = rounded[coord_ind, :]

        neighbor_weights = distance_weights[coord_ind, :]
        neighbor_weights /= neighbor_weights.sum()  # normalize so we dont desaturate
        neighbor_distances = nearest_distances[coord_ind, :]
        for neighbor_ind in range(n_nearest):
            neighbor = neighbor_coords[neighbor_ind, :]
            coord_value = float_img[neighbor[0], neighbor[1], :] * neighbor_weights[neighbor_ind]
            new_image[new_coord[0], new_coord[1], :] += coord_value
            if projection_radius_out:
                dist_weighted = neighbor_distances[neighbor_ind] * neighbor_weights[neighbor_ind]
                projection_error_out[new_coord[0], new_coord[1]] += dist_weighted
    new_image = new_image.astype(image.dtype)

    if projection_radius_out:
        return new_image, projection_error_out
    else:
        return new_image


class InternalCurveInterpolator:
    """  Make num_lines internal lines for sample ratios between curve 0 and curve 1 by sampling along
        the cross lines between each curve and it's nearest point on the other, then ordering all points by total distance
        along both lines.  There two input lines will be also sampled as the 0 and 1 ratio respectively
        :param curve_0:
        :param curve_1:
        :param curve_0_1:
        :param curve_1_0:
        :param num_lines:
    """

    def __init__(self, curve_0: np.ndarray, curve_1: np.ndarray, num_lines: int, inward_direction: Side):
        """

        :param curve_0:
        :param curve_1:
        :param num_lines:
        """
        ratio_gap = 1 / (num_lines + 1)
        self._ratios = np.arange(0, 1 + ratio_gap, ratio_gap)
        self.curve_0_original = curve_0
        self.curve_1_original = curve_1
        curve_0 = np.copy(curve_0)
        curve_1 = np.copy(curve_1)
        curve_0_full = np.copy(curve_0)
        curve_1_full = np.copy(curve_1)
        # make the curves closed so that we can later treat them as a closed shape
        if ~ np.isclose(curve_0[0, :], curve_1[0, :]).all():
            new_point0 = ((curve_0[0, :] + curve_1[0, :]) / 2).reshape(1, -1)
            curve_0_full = np.vstack([new_point0, curve_0])
            curve_1_full = np.vstack([new_point0, curve_1])
        if ~ np.isclose(curve_0[-1, :], curve_1[-1, :]).all():
            new_point_end = ((curve_0[-1, :] + curve_1[-1, :]) / 2).reshape(1, -1)
            curve_0_full = np.vstack([curve_0, new_point_end])
            curve_1_full = np.vstack([curve_1, new_point_end])

        self.curve_0_full = curve_0_full
        self.curve_1_full = curve_1_full

        self._bound_poly = np.vstack([self.curve_0_full, np.flipud(self.curve_1_full)])
        _, curve_0_1_points = get_all_curve_intersections(curve_0, curve_1, on_side=inward_direction)
        _, curve_1_0_points = get_all_curve_intersections(curve_1, curve_0, on_side=other_side(inward_direction))

        # lock the curves to start and end together, may want to make this more general
        self.curve_0_1_points = curve_0_1_points
        self.curve_1_0_points = curve_1_0_points
        both_lines = np.vstack((np.hstack((curve_0, curve_0_1_points)),
                                np.hstack((curve_1_0_points, curve_1))))

        unique_segments, unique_indexes = np.unique(both_lines, axis=0, return_index=True)
        distances_along_0_1 = get_all_distances_along(both_lines[unique_indexes, :2], curve_0)
        distances_along_1_0 = get_all_distances_along(both_lines[unique_indexes, 2:], curve_1)

        distances_along_0_unique = distances_along_0_1
        distances_along_1_unique = distances_along_1_0

        # sort now for all lines to come
        self.both_lines_orig = both_lines
        self.unique_segments = unique_segments

        unique_segments = both_lines[unique_indexes, :]
        self._ratio_lines = []
        sorted_inds0 = self._sort_order_inds(distances_along_0_unique, distances_along_1_unique, 1)
        sorted_inds1 = self._sort_order_inds(distances_along_0_unique, distances_along_1_unique, 0)

        new_curve_0 = unique_segments[sorted_inds0, :2]
        new_curve_1 = unique_segments[sorted_inds1, 2:]

        self._ratio_lines.append(new_curve_0)  # first is curve 0
        for ratio in self._ratios[1:-1]:
            inverse_ratio = 1 - ratio
            combined_line = inverse_ratio * new_curve_0 + ratio * new_curve_1
            self._ratio_lines.append(combined_line)

        self._ratio_lines.append(new_curve_1)  # last is curve 1
        # drop duplicate points:
        for ind in range(len(self._ratio_lines)):
            line = self._ratio_lines[ind]
            # unique, inds = np.unique(line, return_index=True, axis=0)
            # self._ratio_lines[ind] = line[np.sort(inds), :]
        self._lengths = []
        for ratio_line in self._ratio_lines:
            self._lengths.append(euclidean(ratio_line[:-1, :], ratio_line[1:, :]).sum())

    @classmethod
    def _sort_order_inds(cls, distances_along_0_1, distances_along_1_0, ratio):
        inverse_ratio = 1 - ratio
        sort_order_distance = distances_along_0_1 * ratio + distances_along_1_0 * inverse_ratio
        sorted_inds = np.argsort(sort_order_distance)
        return sorted_inds

    def get_inside_points(self) -> np.ndarray:
        """ All of the points inside the bounding polygon.

        :return:
        """
        inside_points = inside_polygon(self._bound_poly)
        return inside_points

    def all_ratio_line_coordinates(self, verbose: bool = False) -> Tuple[
        np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """ Retrieve ratio line coordinates for  each set of points inside

        :param verbose: control printing
        :return: points calculated for (original), new coordinates, manifold distances (left, right),
                distances along (left, right)
        """
        inside_points = self.get_inside_points()

        all_coordinates, distances_along, manifold_distances = self.calculate_projection(inside_points, verbose)

        return inside_points, all_coordinates, manifold_distances, distances_along

    def calculate_projection(self, inside_points: np.ndarray, verbose: bool) -> \
            Tuple[np.ndarray, np.ndarray, np.ndarray]:

        """ Retrieve ratio line coordinates for  each set of points inside

        :param inside_points: ndarray  points
        :param verbose: control printing
        :return: points calculated for (original), new coordinates, manifold distances (left, right),
                distances along (left, right)
        """
        all_coordinates = np.zeros_like(inside_points, dtype=float)
        manifold_distances = np.zeros_like(inside_points)
        distances_along = np.zeros_like(inside_points, dtype=float)
        verb_print(verbose, 'Calculating ratio line coordinates')
        prind = VerbProgPercent(verbose, inside_points.shape[0])
        for index in range(inside_points.shape[0]):
            index_point = inside_points[index, :]
            all_coordinates[index, 0], all_coordinates[index, 1], \
            manifold_distances[index, 0], manifold_distances[index, 1], \
            distances_along[index, 0], distances_along[index, 1] = \
                self.ratio_line_coordinates(index_point)
            prind.update()
        return all_coordinates, distances_along, manifold_distances

    def all_boundary_points_coordinates(self, verbose: bool = False) -> Tuple[
        np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """ Retrieve ratio line coordinates for each bounding polygon vertex

        :param verbose: control printing
        :return: points calculated for (original), new coordinates, manifold distances (left, right),
                distances along (left, right)
        """
        all_coordinates, distances_along, manifold_distances = self.calculate_projection(self._bound_poly, verbose)

        return self._bound_poly, all_coordinates, manifold_distances, distances_along

    def all_curves_coordinates(self, verbose: bool = False) -> Tuple[
        np.ndarray, np.ndarray]:
        """ Retrieve ratio line coordinates for each of the original curves

        :param verbose: control printing
        :return: new coordinates 0, new coordinates 1
        """
        new_coordinates_0, _, _ = self.calculate_projection(self.curve_0_original, verbose)
        new_coordinates_1, _, _ = self.calculate_projection(self.curve_1_original, verbose)

        return new_coordinates_0, new_coordinates_1

    @classmethod
    def _manifold_line_distance(cls, check_lines, current_intersection):
        """ Get the distance along the intersecting points of consecutive ratio lines

        :param check_lines:
        :param current_ind:
        :param current_intersection:
        :return: distancec
        """
        distance = 0
        for next_line in check_lines:
            nearest = nearest_segment(current_intersection, next_line)
            distance += nearest[0]
            current_intersection = nearest[1]

        return distance

    def ratio_line_coordinates(self, point: np.ndarray):
        """

        :param point: point to check (1,2)
        :param segment_0: segment in curve 0 intersection is with (used for efficiency)
        :param segment_1: segment in curve 1 intersection is with (used_for_efficiency)
        :return:
        """

        # now find the intersection again with each of the ratio lines within min and max ind
        # then use the minimum distance 2 to interpolate distance
        intersections = []

        for ratio_line in self._ratio_lines:
            # +2 singe itś points in the lines, not segments
            nearest = nearest_segment(point, ratio_line)
            intersections.append(nearest)

        distances_from = np.array([intersect[0] for intersect in intersections])
        intersect_points = np.array([intersect[1] for intersect in intersections])

        nearest_2 = np.argsort(distances_from)[:2]
        if nearest_2[0] < nearest_2[1]:
            nearest_below = nearest_2[0]
            nearest_above = nearest_below + 1
        else:
            nearest_above = nearest_2[0]
            nearest_below = nearest_above - 1

        manifold_distance_below = distances_from[nearest_below]

        # add up below
        current_intersection = intersect_points[nearest_below]

        check_lines = reversed(self._ratio_lines[:nearest_below])
        manifold_distance_below += self._manifold_line_distance(check_lines, current_intersection)

        manifold_distance_above = distances_from[nearest_above]

        current_intersection = intersect_points[nearest_above]
        check_lines = self._ratio_lines[nearest_above + 1:]
        manifold_distance_above += self._manifold_line_distance(check_lines, current_intersection)

        # over 2 because we go from - max dist -> max dist , doubling the span
        x_new = (manifold_distance_above - manifold_distance_below) / 2

        # get normalized lengths, will scale them by outer curve based length in end

        distance_along_below = distance_along(self._ratio_lines[nearest_below], point) / self._lengths[nearest_below]

        distance_along_above = distance_along(self._ratio_lines[nearest_above], point) / self._lengths[nearest_above]

        expected_final_length = (self._lengths[0] + self._lengths[-1]) / 2
        y_new = (distance_along_below + distance_along_above) / 2 * expected_final_length
        return x_new, y_new, manifold_distance_below, manifold_distance_above, distance_along_below, distance_along_above


def deoverlap_curves(curve_outer: np.ndarray, curve_inner: np.ndarray, on_side: Side) -> np.ndarray:
    """ If curve1 crosses curve0, it will be nudged to be at least 0.5 pixels inside it.

    :param curve_outer:
    :param curve_inner:
    :return: corrected curve1
    """

    curve_outer_shapely = LineString(curve_outer)

    curve_outer_self_distances = np.asarray(
        [curve_outer_shapely.project(Point(curve_outer[i, :])) for i in range(curve_outer.shape[0])])

    curve1_points = [curve_inner[i, :] for i in range(curve_inner.shape[0])]
    # if start/end on same, skip since that's expcted
    start_offset = 0 if ~(np.isclose(curve_inner[0, :], curve_outer[0, :]).all()) else 1
    end_offset = 1 if ~(np.isclose(curve_inner[-1, :], curve_outer[-1, :]).all()) else 2
    curve1_index = start_offset
    while curve1_index < len(curve1_points) - end_offset :

        as_segment = LineString(curve1_points[curve1_index:curve1_index + 2])
        intersection = as_segment.intersection(curve_outer_shapely)
        if not intersection.is_empty:
            # if it overlaps curve 0
            #   order intersections and any curve0 points in between by distance along curve0
            #   add each to curve1
            # replace end point with next point on curve 0 after the overlap. Move all new points 1 pixel to the right
            if isinstance(intersection, Point):
                new_segment = [intersection]
                intersection_distance0 = curve_outer_shapely.project(intersection)
            elif isinstance(intersection, MultiPoint):

                point_dists = np.asarray([curve_outer_shapely.project(geom) for geom in intersection.geoms])
                chosen_ind = np.argmin(point_dists)
                chosen_point = intersection.geoms[chosen_ind]
                new_segment = [np.asarray(chosen_point.xy).flatten()]
                intersection_distance0 = point_dists[chosen_ind]
            curve_outer_greater_ind_list = np.nonzero(curve_outer_self_distances > intersection_distance0)[0]
            if len(curve_outer_greater_ind_list) > 0:
                curve_outer_greater_ind = curve_outer_greater_ind_list[0]

            else:
                curve_outer_greater_ind = -1
            new_segment.append(curve_outer[curve_outer_greater_ind, :])

            intersection_distance1 = curve_outer_self_distances[curve_outer_greater_ind]
            if intersection_distance0 > intersection_distance1:
                new_segment = new_segment[::-1]
            as_shapely_lines = LineString(new_segment)
            # as_shapely_lines = as_shapely_lines.parallel_offset(0.1, on_side.value)
            as_vect_dist = curve_outer[curve_outer_greater_ind] - curve_outer[curve_outer_greater_ind - 1]
            orthog_vec = as_vect_dist / linalg.norm(as_vect_dist) @ ROTATION * 0.1

            new_points = [np.asarray((as_shapely_lines.xy[0][i], as_shapely_lines.xy[1][i])) for i in
                          range(len(new_segment))]
            if on_side == Side.LEFT_SIDE:
                orthog_vec = - orthog_vec
            new_points[0] += orthog_vec
            new_points[1] += orthog_vec
            deletes = True
            current_point = curve1_points[curve1_index]
            while deletes:
                if curve1_index + 1 == len(curve1_points):
                    raise DeoverlapError('Reached end of curve. Is there a complete crossing of curves?')
                next_natural_point = curve1_points[curve1_index + 1]

                next_point_dist = curve_outer_shapely.project(Point(next_natural_point))
                curr_point_dist = curve_outer_shapely.project(Point(current_point))

                if intersection_distance0 > next_point_dist or curve_outer_self_distances[
                    curve_outer_greater_ind] > next_point_dist or next_point_dist <= curr_point_dist:
                    # add in next curve0 point, curve1_points

                    del curve1_points[curve1_index + 1]
                else:
                    deletes = False
            curve1_points[curve1_index + 1:curve1_index + 1] = new_points[::]
            curve1_index += len(new_segment)

        else:
            curve1_index += 1

    curve1_new = np.asarray(curve1_points)
    _, inds = np.unique(curve1_new, return_index=True, axis=0)
    curve1_new = curve1_new[np.sort(inds)]
    if curve_outer_shapely.intersects(LineString(curve1_new[start_offset:-end_offset, :])):
        raise DeoverlapError("Curves still intersect. Check that curve1 does not cross "
                             "the object and overlap curve0.")
    return curve1_new
